import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectWithFilterPage } from './select-with-filter.page';

describe('SelectWithFilterPage', () => {
  let component: SelectWithFilterPage;
  let fixture: ComponentFixture<SelectWithFilterPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectWithFilterPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectWithFilterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
