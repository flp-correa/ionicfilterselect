import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

export class FilterList {
  label: string;
  value: any;
}

@Component({
  selector: 'app-select-with-filter',
  templateUrl: './select-with-filter.component.html',
  styleUrls: ['./select-with-filter.component.scss'],
})
export class SelectWithFilterComponent implements OnInit {

  descLabel = '';

  filterList: FilterList[];
  filterListSearch: any[];

  constructor(private navParams: NavParams, private modal: ModalController) { }

  ngOnInit() {
    this.filterList = this.navParams.get('filterList');
    this.filterListSearch = this.filterList;
  }

  search() {
    this.filterListSearch = this.filterList;
    const regexLabel = new RegExp(this.descLabel, 'i');
    if (this.descLabel !== '') {
      this.filterListSearch = this.filterList.filter((element) => regexLabel.test(element.label));
    }
  }

  elementSelected(element) {
    this.modal.dismiss(element);
  }

}
