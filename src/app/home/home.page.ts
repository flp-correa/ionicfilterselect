import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SelectWithFilterComponent } from '../select-with-filter/select-with-filter.component';
import { OverlayEventDetail } from '@ionic/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  cidade = {label: '', value: null};

  constructor(private modalController: ModalController) {}

  async showCidadesSelect() {
    const cidades = [
      {label: "Florianópolis", value: 1},
      {label: "Joinville", value: 2},
      {label: "Imbituba", value: 3},
      {label: "Blumenau", value: 4},
      {label: "São José", value: 5},
      {label: "Imbituba", value: 6},
      {label: "Florianópolis", value: 7},
      {label: "Joinville", value: 8},
      {label: "Imbituba", value: 9}
    ]
    const modal = await this.modalController.create({
      component: SelectWithFilterComponent,
      componentProps: {filterList: cidades},
      cssClass: 'teste',

    });

    modal.onWillDismiss().then((eventDetail: OverlayEventDetail) => {
      if (eventDetail.data) {
        this.cidade = eventDetail.data;
      }
    })

    return await modal.present();
  }
}
